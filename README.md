from command line:

1. clone the repository: git clone https://gitlab.com/lucatabone/mia-rover-challenge
2. cd into the source directory
3. type into your command line: python main.py "5 5-1 2 N-LMLMLMLMM-3 3 E-MMRMMRMRRM"

The expected outout should be:
1 3 N
5 1 E


NOTE that the separator used for the input string must be a dash "-" when you call main.py from command line.
