import unittest
from master import Master


class StandardCase(unittest.TestCase):
    def setUp(self):
        self.instructions = ["5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM"]
        self.rover_1_target_state = "1 3 N"
        self.rover_2_target_state = "5 1 E"
        self.expected_master_state = [self.rover_1_target_state, self.rover_2_target_state]

    def test_rover_end_positions(self):
        master = Master()
        master.execute_instructions('-'.join(self.instructions))
        self.assertEqual(master.rovers[0].state, self.rover_1_target_state)
        self.assertEqual(master.rovers[1].state, self.rover_2_target_state)
        self.assertEqual(master.state, self.expected_master_state)


class NoPlateauCase(unittest.TestCase):
    def setUp(self):
        self.instructions = ["0 0", "0 0 N", "RMMMLLMMLMMRMM", "0 0 S", "MMMRRMLML"]
        self.rover_1_target_state = "0 0 W"
        self.rover_2_target_state = "0 0 S"

    def test_rover_positions(self):
        master = Master()
        master.execute_instructions('-'.join(self.instructions))
        self.assertEqual(master.rovers[0].state, self.rover_1_target_state)
        self.assertEqual(master.rovers[1].state, self.rover_2_target_state)


class InitialRoverPositionExceedsPlateauCase(unittest.TestCase):
    def setUp(self):
        self.instructions = ["0 0", "0 1 N", "M"]

    def test_assertion_error(self):
        master = Master()
        with self.assertRaises(AssertionError):
            master.execute_instructions('-'.join(self.instructions))


if __name__ == "__main__":
    unittest.main()
