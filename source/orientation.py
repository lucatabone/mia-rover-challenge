id_to_orientation = {
    0: "N",
    1: "E",
    2: "S",
    3: "W"
}

orientation_to_id = {
    "N": 0,
    "E": 1,
    "S": 2,
    "W": 3
}
