from rover import Rover
from plateau import Plateau


class Master:
    def __init__(self):
        self.plateau = None
        self.rovers = list()

    @property
    def state(self):
        res = []
        append = res.append
        for i, rover in enumerate(self.rovers):
            append(rover.state)
        return res

    def repr_rovers_state(self):
        print('\n'.join(self.state))

    def execute_instructions(self, instructions: str):
        self.rovers = list()
        parts = instructions.split('-')
        self.initialize_plateau(parts[0])
        assert len(parts) % 2 == 1
        amount_rovers = int((len(parts) - 1) / 2)
        for rover_id in range(amount_rovers):
            rover = self.initialize_rover(position_str=parts[2 * rover_id + 1])
            rover.process_movements(movement_str=parts[2 * rover_id + 2])

    def initialize_plateau(self, properties: str):
        dimensions = properties.split(' ')
        self.plateau = Plateau(
            width=int(dimensions[0]),
            length=int(dimensions[1])
        )

    def initialize_rover(self, position_str: str):
        position_parts = position_str.split(' ')
        rover = Rover(
            plateau=self.plateau,
            pos_x=int(position_parts[0]),
            pos_y=int(position_parts[1]),
            orientation=position_parts[2]
        )
        self.rovers.append(rover)
        return rover
