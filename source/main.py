from sys import argv
from master import Master


if __name__ == '__main__':
    master = Master()
    master.execute_instructions(instructions=argv[1])
    master.repr_rovers_state()
