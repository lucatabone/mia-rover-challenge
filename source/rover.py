from plateau import Plateau
from orientation import id_to_orientation, orientation_to_id


class Rover:
    def __init__(self, pos_x: int, pos_y: int, orientation: str, plateau: Plateau):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.plateau = plateau
        assert self.pos_x <= plateau.width
        assert self.pos_y <= plateau.length
        self.orientation_id = orientation_to_id[orientation]

    @property
    def orientation(self):
        return id_to_orientation[self.orientation_id]

    @property
    def state(self):
        return '{} {} {}'.format(self.pos_x, self.pos_y, self.orientation)

    def turn_left(self):
        self.orientation_id = (self.orientation_id - 1) % 4

    def turn_right(self):
        self.orientation_id = (self.orientation_id + 1) % 4

    def process_movements(self, movement_str: str):
        for movement in movement_str:
            if movement == 'L':
                self.turn_left()
            elif movement == 'R':
                self.turn_right()
            elif movement == 'M':
                self.move_one_step()

    def move_one_step(self):
        if self.orientation_id == 0 and self.pos_y < self.plateau.length:
            self.pos_y += 1
        elif self.orientation_id == 1 and self.pos_x < self.plateau.width:
            self.pos_x += 1
        elif self.orientation_id == 2 and self.pos_y > 0:
            self.pos_y -= 1
        elif self.pos_x > 0:
            self.pos_x -= 1
